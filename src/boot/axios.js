import Vue from 'vue'
import axios from 'axios'

export default async () => {
  axios.defaults.baseURL = process.env.API
  Vue.prototype.$axios = axios
}
