
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'home', component: () => import('pages/categories/index.vue') },
      { path: '/categories/:id', props: true, name: 'category.show', component: () => import('pages/categories/show.vue') }
    ]
  },
  {
    path: '/questions',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/questions/:id', props: true, name: 'question.show', component: () => import('pages/questions/show.vue') }
    ]
  },
  {
    path: '/tests',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/tests/', props: true, name: 'tests.show', component: () => import('pages/tests/show.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
